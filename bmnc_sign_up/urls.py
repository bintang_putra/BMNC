from django.conf.urls import url
from .views import index,index2

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^index2/$', index2, name='index2')
]
