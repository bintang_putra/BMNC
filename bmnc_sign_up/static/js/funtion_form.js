$(document).ready(function(){
  var counter = [];
    $("#roleuser").change(function(){
        var selectedUser = $("#roleuser option:selected").val();
        counter.push(selectedUser);
        if(counter.length==1){
          if(selectedUser=='Mahasiswa'){
            var html = '<label for="status"><span class="fa fa-id-badge" style="font-size:18pt"></span></label>\
            <input type="text" class="form-control" id="status" placeholder="Enter your academic status" required="true">'
            $("#additionalinput").append(html)
          }
          else{
            var html = '<label for="iduniv">ID University:</label>\
            <select class="form-control" id="iduniv" style="height:50px" required="true">\
              <option>Select ID --</option>\
              <option value="1560695">UI</option>\
              <option value="100318">ITB</option>\
              <option value="1381081">UGM</option>\
            </select>'
            $("#additionalinput").append(html)
          }
        }
        else{
          if(selectedUser=='Mahasiswa'){
            var html = '<label for="status"><span class="fa fa-id-badge" style="font-size:18pt"></span></label>\
            <input type="text" class="form-control" id="status" placeholder="Enter your academic status" required="true">'
            $("#additionalinput").empty()
            $("#additionalinput").append(html)
          }
          else{
            var html = '<label for="iduniv">ID University:</label>\
            <select class="form-control" id="iduniv" required="true">\
              <option>Select ID --</option>\
              <option value="1560695">UI</option>\
              <option value="100318">ITB</option>\
              <option value="1381081">UGM</option>\
            </select>'
            $("#additionalinput").empty()
            $("#additionalinput").append(html)
          }
        }
    });
});
