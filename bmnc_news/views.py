from django.shortcuts import render

response = {'author': 'BMNC14B'}
def index(request) :
    html = 'bmnc_news/news.html'
    return render(request, html, response)

def history(request) :
    html = 'bmnc_news/riwayat.html'
    return render(request, html, response)

def profile(request) :
    html = 'bmnc_news/profile.html'
    return render(request, html, response)
