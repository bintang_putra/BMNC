from django.conf.urls import url
from .views import index, history, profile

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^history/$', history, name='history'),
    url(r'^profile/$', profile, name='profile')

]

